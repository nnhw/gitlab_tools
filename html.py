from yattag import Doc

def generate_text_lists(header_list,data_lists):

    doc, tag, text = Doc().tagtext()

    with tag('html'):
        with tag('body'):
            for i,header in enumerate(header_list):
                with tag('h1'):
                    text(header)
                for data in data_lists[i]:    
                    with tag('br'):
                        text(data)

    result = doc.getvalue()
    return result

def generate_link_lists(header_list,data_lists):

    doc, tag, text = Doc().tagtext()

    with tag('html'):
        with tag('body'):
            for i,header in enumerate(header_list):
                with tag('h1'):
                    text(header)
                for link_text, link in data_lists[i]:    
                    with tag('a'):
                        text(link_text)
                        doc.attr(href = link)
                    with tag('br'):
                        None

    result = doc.getvalue()
    return result
