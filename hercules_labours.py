import subprocess
import os
import pathlib
import re

import gitlab_tools.api

# TODO: add group_id_list

def run_analysis(workdir, start_date, end_date, group_id, filters, token):

        print("Starting hercules analysis")

        INVOKE_PATH = workdir
        ANALYSIS_PATH = INVOKE_PATH + "/analysis"
        RESULT_PATH = INVOKE_PATH + "/results"

        START_DATE = start_date
        END_DATE = end_date

        projects = []
        folder = []
        result_files_list = []

        print("Working directory is ", INVOKE_PATH)

        print("We need a developers.txt list, let's test if it exists in the workdir")
        command = subprocess.run(
            ["ls", INVOKE_PATH+"/developers.txt"], stdout=subprocess.DEVNULL)
        if (command.returncode == 0):
                print("developers.txt test successfull")
        else:
                print("developers.txt test failed on code %d, exiting" %command.returncode)
                return(1)

        projects = gitlab_tools.api.get_projects_from_group_recursive(group_id, token)
        print("%d projects found" % len(projects))

        # FILTERING
        print("Filtering out projects")
        pattern = re.compile(filters) 
        projects[:] = [project for project in projects if not pattern.search(
            project['path_with_namespace'])]
        print("%d Files left" % len(projects))

        for project in projects:
                pathlib.Path(
                    ANALYSIS_PATH + "/" + project['full_path']).mkdir(parents=True, exist_ok=True)

        print("Starting hercules analyse process")
        for project in projects:
                print("Analysing " + project['name'])
                cmd = "hercules --burndown --burndown-people --devs --people-dict=developers.txt --pb " + \
                    "https://" + "oauth2:" + token + "@" + "gitlab.com/" + \
                        project['path_with_namespace'] + ".git"

                # disabled, because of the tensorflow and bablefish dependency
                # cmd = "docker run -v" + INVOKE_PATH + ":/io/src --rm srcd/hercules hercules --burndown --burndown-files --burndown-people --couples --shotness --devs --people-dict=/io/src/developers.txt --pb " + "https://"+ "oauth2:" + gl.private_token +"@" + "gitlab.com/" + project.path_with_namespace + ".git"
                cmd_splitted = cmd.split()
                file = open(ANALYSIS_PATH+"/"+project['path_with_namespace']+".pb", "w")
                pb = subprocess.run(cmd_splitted,shell=False, stdout=file)
                file.close()

        print("Generating results list")
        pattern = re.compile(".pb")

        for i in os.walk(ANALYSIS_PATH):
            folder.append(i)

        for address, dirs, files in folder:
            for file in files:
                    if pattern.search(file):
                        result_files_list.append(address+'/'+file)

        print("%d Files found" % len(result_files_list))
        # print(result_files_list)

        print("Starting hercules results combining process")
        result_files_string = " ".join(result_files_list)
        cmd = "hercules combine " + result_files_string
        # print(result_files_string)
        cmd_splitted = cmd.split()
        file = open(ANALYSIS_PATH+"/all.pb", "w")
        pb = subprocess.run(cmd_splitted,shell=False, stdout=file)
        file.close()

        pathlib.Path(RESULT_PATH).mkdir(parents=True, exist_ok=True)

        print("Starting labours for plot generation")
        cmd = "labours -f pb -i " + ANALYSIS_PATH+"/all.pb --style seaborn-poster"
        fmt = ".png"
        start_date = " --start-date "+START_DATE
        end_date = " --end-date "+END_DATE
        fnt_size_def = " --font-size 16"

        cmd_list = [
        cmd + fnt_size_def + " -m devs " + start_date + end_date + " -o " + RESULT_PATH+"/developers" + fmt,
        cmd + fnt_size_def + " -m ownership " + start_date + end_date + " -o "+RESULT_PATH+"/ownership" + fmt,
        cmd + fnt_size_def + " -m burndown-project " + start_date + end_date + " -o " + RESULT_PATH+"/burndown_project" + fmt,
        cmd + " -m burndown-project " + start_date + end_date + " --resample M --font-size 8 -o " + RESULT_PATH+"/burndown_project_month" + fmt,
        # TODO: disable http result posting
        # cmd + fnt_size_def + " -m overwrites-matrix -o RESULT_PATH/overwrites-matrix" + fmt,
        cmd + fnt_size_def + " -m old-vs-new " + start_date + end_date + " -o " + RESULT_PATH+"/old_vs_new" + fmt,
        cmd + fnt_size_def + " -m languages " + "-o " + RESULT_PATH+"/languages.txt",
        cmd + fnt_size_def + " -m burndown-person " + start_date + end_date + " -o " + RESULT_PATH+"/burndown_person" + fmt,
        cmd + " -m burndown-person " + start_date + end_date + " --resample M --font-size 8 -o " + RESULT_PATH+"/burndown_person_month" + fmt,
        # cmd + fnt_size_def + " -m devs-parallel -o RESULT_PATH/devs_parallel" + fmt,
        ]

        for cmd in cmd_list:
            subprocess.run(cmd.split(),shell=False)

        return 0
