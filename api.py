import requests

def get_subgroups_from_group(group_id, token):
    subgroups = []
    response = requests.get(
    'https://gitlab.com/api/v4/groups/'+str(group_id)+'/descendant_groups',
    params={},
    headers={'PRIVATE-TOKEN': token},
    )
 
    json_response = response.json()
    for subgroup in json_response:
        trimmed_subgroup = {'id': subgroup['id'], 'web_url': subgroup['web_url']}
        subgroups.append(trimmed_subgroup)

    return subgroups

# https://docs.gitlab.com/ee/api/issues.html#list-group-issues
def get_user_issues_closed_by_milestone_and_labels(group_id, milestone, labels, assignee_id, token):
    response = requests.get(
    'https://gitlab.com/api/v4/groups/'+str(group_id)+'/issues/',
    params={'assignee_id': assignee_id,
    'milestone' : milestone,
    'state' : 'closed',
    'labels': labels
    },
    headers={'PRIVATE-TOKEN': token},
    )
    issue_list = response.json()

    return issue_list

def get_user_issues_statistics_by_milestone(group_id, milestone, assignee_username, token):
    response = requests.get(
    'https://gitlab.com/api/v4/groups/'+str(group_id)+'/issues_statistics/',
    params={'assignee_username': assignee_username,
    'milestone' : milestone,
    },
    headers={'PRIVATE-TOKEN': token},
    )
 
    json_response = response.json()
    stats = { 'closed':json_response['statistics']['counts']['closed'],'opened':json_response['statistics']['counts']['opened'] }
    return stats

def get_user_issues_statistics_by_time(group_id, time_after, time_before , assignee_id, token):
    response = requests.get(
    'https://gitlab.com/api/v4/groups/'+str(group_id)+'/issues_statistics/',
    params={'assignee_id': assignee_id,
    'updated_after' : time_after,
    'updated_before' : time_before
    },
    headers={'PRIVATE-TOKEN': token},
    )
 
    json_response = response.json()
    stats = { 'closed':json_response['statistics']['counts']['closed'],'opened':json_response['statistics']['counts']['opened'] }
    return stats

def get_user_events_number(user_id, time_after, time_before,  token):
    page_events_number = None
    page_events_total = 0 
    page = 1

    while page_events_number != 0:

        response = requests.get(
        'https://gitlab.com/api/v4/users/'+str(user_id)+'/events',
        params={'after':time_after,
        'before':time_before,
        'per_page':100,
        'page':page
        },
        headers={'PRIVATE-TOKEN': token},
        )
        json_response = response.json()
        page_events_number = len(json_response)
        page_events_total = page_events_total + page_events_number
        page = page + 1

    return page_events_total 

def get_users_from_group(group_id, token):
    members = []
    response = requests.get(
    'https://gitlab.com/api/v4/groups/'+str(group_id)+'/members/all',
    params={},
    headers={'PRIVATE-TOKEN': token},
    )
 
    json_response = response.json()
    # for member in json_response:
    #     trimmed_member = {'id': member['id'], 'name': member['name']}
    #     members.append(trimmed_member)

    return json_response

def get_users_from_group_recursive(group_id, token):
    members_list = []
    members_list = members_list + get_users_from_group(group_id,token)

    subgroups = get_subgroups_from_group(group_id,token)

    for subgroup in subgroups:
        members_list = members_list + get_users_from_group(subgroup['id'],token)

    unique_members_list = list({v['id']:v for v in members_list}.values())

    return unique_members_list

def get_milestones_from_group(group_id, search_string, token):
    milestones = []
    response = requests.get(
    'https://gitlab.com/api/v4/groups/'+str(group_id)+'/milestones',
    params={'search': search_string},
    headers={'PRIVATE-TOKEN': token},
    )
 
    json_response = response.json()
    for milestone in json_response:
        trimmed_milestone = {'id': milestone['id'], 'title': milestone['title']}
        milestones.append(trimmed_milestone)

    return milestones

def get_milestones_from_group_recursive(group_id, search_string, token):
    milestones_list = []
    milestones_list = milestones_list + get_milestones_from_group(group_id,search_string,token)

    subgroups = get_subgroups_from_group(group_id,token)

    for subgroup in subgroups:
        milestones_list = milestones_list + get_milestones_from_group(subgroup['id'],search_string,token)

    return milestones_list

def get_projects_from_group(group_id, token):
    projects = []
    response = requests.get(
    'https://gitlab.com/api/v4/groups/'+str(group_id)+'/projects',
    params={'archived':'false'},
    headers={'PRIVATE-TOKEN': token},
    )
 
    json_response = response.json()
    for project in json_response:
        trimmed_project = {'id': project['id'], 'name': project['name'], 'path_with_namespace': project['path_with_namespace'], 'full_path': project['namespace']['full_path']}
        projects.append(trimmed_project)

    return projects    

def get_projects_from_group_recursive(group_id, token):
    projects_list = []
    projects_list = projects_list + get_projects_from_group(group_id,token)

    subgroups = get_subgroups_from_group(group_id,token)

    for subgroup in subgroups:
        projects_list = projects_list + get_projects_from_group(subgroup['id'],token)

    return projects_list

def get_users_issue_stats_of_group(group_id,time_after, time_before, token):
    members_list = get_users_from_group_recursive(group_id,token)

    stats = []

    for member in members_list:
        issue_stats = get_user_issues_statistics_by_time(group_id,time_after,time_before,member['id'],token)
        stats.append({'name':member['name'],'stat':issue_stats})

    return stats

def get_users_events_count_of_group(group_id,time_after,time_before, token):
    members_list = get_users_from_group_recursive(group_id,token)

    stats = []

    for member in members_list:
        event_stats = get_user_events_number(member['id'],time_after,time_before,token)
        stats.append({'name':member['name'],'activity':event_stats})

    return stats

#untested
def get_users_issues_closed_of_group_in_milestone(group_id, milestone, labels, token):
    members_list = get_users_from_group_recursive(group_id,token)

    closed_issue_list = []
    for member in members_list:
        closed_issue_list = closed_issue_list + get_user_issues_closed_by_milestone_and_labels(group_id, milestone, labels, member['id'], token)

    return closed_issue_list
